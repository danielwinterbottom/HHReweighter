wget https://launchpad.net/mg5amcnlo/lts/2.9.x/+download/MG5_aMC_v2.9.22.tar.gz -O MG5_aMC_v2.9.22.tar.gz
tar -xvf MG5_aMC_v2.9.22.tar.gz

cd MG5_aMC_v2_9_22

echo "install oneloop; install ninja ; install collier;" | python bin/mg5_aMC

wget https://gitlab.com/danielwinterbottom/twosinglet/-/archive/master/twosinglet-master.tar.gz?path=loop_sm_twoscalar -O loop_sm_twoscalar.tar.gz

tar -xvf loop_sm_twoscalar.tar.gz

mv twosinglet-master-loop_sm_twoscalar/loop_sm_twoscalar models/.
rm -r twosinglet-master-loop_sm_twoscalar
rm loop_sm_twoscalar.tar.gz

python3 bin/mg5_aMC -f ../scripts/mg_script

cp ../cards/param_card_sm.dat SM_hh/Cards/param_card.dat

cd ..
