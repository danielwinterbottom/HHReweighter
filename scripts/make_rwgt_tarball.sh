MG_DIR=$1
E=$2

if [ $E == "13" ]; then
  ebeam1="set ebeam1 6500"
  ebeam2="set ebeam2 6500"
elif [ $E == "14" ]; then
  ebeam1="set ebeam1 7000"
  ebeam2="set ebeam2 7000"
elif [ $E == "13p6" ]; then
  ebeam1="set ebeam1 6800"
  ebeam2="set ebeam2 6800"
fi

dir=$PWD

python3 scripts/make_reweight_card.py
cp reweight_card.dat ${MG_DIR}/SM_hh/Cards/.
cd ${MG_DIR}/SM_hh
{ echo 0; echo set nevents 100; echo set gridpack True; echo ${ebeam1}; echo ${ebeam2}; } | python3 bin/generate_events SM_hh
cd ../../
mkdir -p gridpack_SM_hh
cd gridpack_SM_hh
tar -xvf ../${MG_DIR}/SM_hh/SM_hh_gridpack.tar.gz 
mkdir -p madevent/Events/pilotrun
cd madevent
rm -r rwgt_${E}TeV
cp Cards/reweight_card.dat Cards/reweight_card.dat.backup
{ echo reweight=OFF ; echo 0; echo set nevents 1 ; echo set gridpack False; echo set use_syst False ; } | bin/generate_events template_lhe
cp Events/template_lhe/unweighted_events.lhe.gz Events/pilotrun/.
sed -n '/^launch/q;p' Cards/reweight_card.dat.backup > Cards/reweight_card.dat
echo "launch" >> Cards/reweight_card.dat
echo "0" | ./bin/madevent --debug reweight pilotrun
cp Cards/reweight_card.dat.backup Cards/reweight_card.dat
mv rwgt rwgt_${E}TeV

echo Making tarball
tar zcf rwgt_${E}TeV.tar.gz rwgt_${E}TeV 

echo Reweighting tarball created: ${PWD}/rwgt_${E}TeV.tar.gz 
