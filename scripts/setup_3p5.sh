wget https://launchpad.net/mg5amcnlo/3.0/3.5.x/+download/MG5_aMC_v3.5.4.tar.gz https://launchpad.net/mg5amcnlo/3.0/3.5.x/+download/MG5_aMC_v3.5.5.tar.gz -O MG5_aMC_v3.5.5.tar.gz
tar -xvf MG5_aMC_v3.5.5.tar.gz

cd MG5_aMC_v3_5_5

echo "install oneloop; install ninja ; install collier;" | python bin/mg5_aMC

wget https://gitlab.com/danielwinterbottom/twosinglet/-/archive/master/twosinglet-master.tar.gz?path=loop_sm_twoscalar -O loop_sm_twoscalar.tar.gz

tar -xvf loop_sm_twoscalar.tar.gz

mv twosinglet-master-loop_sm_twoscalar/loop_sm_twoscalar models/.
rm -r twosinglet-master-loop_sm_twoscalar
rm loop_sm_twoscalar.tar.gz

python bin/mg5_aMC -f ../scripts/mg_script_3p5

cp ../cards/param_card_sm.dat SM_hh/Cards/param_card.dat

cd ..
