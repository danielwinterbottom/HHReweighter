# HHReweighter

This tool uses matrix element reweighting to account for inteference effects between resonant and non-resonant di-Higgs processes.
More details can be found in arXiv:2409.06651.

If you use the tool please cite these two papers:

- F Feuerstake, E Fuchs, T Robens, D Winterbottom arXiv:2409.06651

- A Papaefstathiou, T Robens and G Tetlalmatzi-Xolocotzi JHEP05(2021)193

## Tutorial

There is a worked example showing how to use the code in this Google Colab notebook: https://colab.research.google.com/drive/1kqu-9ouFCXNKJFB8dC0OHUdpsk1BxtiR?usp=sharing 

## Setup

Clone repository:

	git clone git@gitlab.com:danielwinterbottom/HHReweighter.git

In order to use the tool you first need to untar the reweighting tarballs for the COM energy you intend to use, e.g:

	tar -xvf rwgt_13TeV.tar.gz

## 

The repository contains classes to perform a "model-independent" reweighting and a "model-dependent" reweighting

The class used for model-independent reweighting ("HHReweight") is defined in python/reweight.py.
In the model-independent case the uses must specify a set of target masses and (relative) widths for the reweighting.
The tool will then output weights for several components of the full distribution. There are 3 components that describe the SM-like terms (box-diagram, triangle s-channel diagram - dependent on SM-like triple Higgs coupling, and the inteference between the box and triangle).
Then for each combination of mass and width values the tool returns 3 additional weights. One weight corresponds to the resonant s-channel diagram, while the other 2 weights correspond to the inteference between the resonant diagram and the SM-liek box, and triangle diagrams.
All weights are produced assuming SM-like Yukawa and trilinear (lambda) couplings. For the BSM H this means that the Yukawa couplings equal the SM values and the lambda_Hhh trilinear couplings equals the SM lambda.  
The main idea is to store weights for different mass and width points which can then be combined to describe any di-Higgs model by scaling the individual templates by the couplings predicted in said model.
A given model will predict the values of the top Yukawa couplings for the h and H, the trilinear couplings lam_hhh and lambda_Hhh, and the H width (the mH mass may also be a prediction of the model, or can be one of the input parameters). You can then use the predictions for the couplings to scale the individual templates 

The model-dependent case is so far set up to reweight to different parameter points in the singlet model. The class used for model-dependent reweighting ("HHReweightSinglet") is defined in python/reweight_singlet.py.

## Reweighting an LHE File

The reweight_lhe_file.py script will reweight an LHE file creating a new LHE file with the weights appended:

	python examples/reweight_lhe_file.py -i examples/unweighted_events_sm_hh.lhe.gz -o output_lhe.lhe

This will work for any LO or NLO LHE file. However, for an NLO LHE file, the results will be approximate. 
In this case the reweighting ignores the incoming and outgoing partons, and uses the di-Higgs events to construct the incoming gluons to be used in the ME calculations.  

## Reproducing Madgraph reweighting tarballs

To reproduce tarballs you first need to setup Madgraph and clone to model
This script will set everything up for Madgraph version 3.5.4:

	./scripts/setup.sh

Reporduce the reweighting tarballs:

	for E in 13 13p6 14; do ./scripts/make_rwgt_tarball.sh MG5_aMC_v2_9_22 $E; done

Copy the new gridpacks to the main directory

	cp gridpack_SM_hh/madevent/rwgt_1*TeV.tar.gz .

# Troubleshooting

 - In case of issues related to python failing to find "reweight" or "allmatrix2py" modules, make sure you add the paths to HHReweighter and HHReweighter/python to ypur PYTHONPATH environmental variable, e.g:
    PYTHONPATH=$PYTHONPATH:/path/to/HHReweighter/:/path/to/HHReweighter/python 

 - If you have issues due to compatability with libraries etc it might be necessary to reproduce the reweighting tarballs yourself, follow the instruction under "Reproducing Madgraph reweighting tarballs" to do this 
