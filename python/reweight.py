import math
import copy
import os
import sys
import glob
if sys.version_info.major == 3:
    import importlib.util
else:
    import imp

class HHReweight:

    def __init__(self, masses=[], widths=[], E='13TeV', ReweightSChan=False, RefMassRelWidth=(), mass_widths_dict=None):
        """
        Initialize the reweighting module with specified parameters.

        Parameters:
        - masses (list, optional): List of masses for resonance reweighting. Default is an empty list.
        - widths (list, optional): List of relative widths for resonance reweighting. Default is an empty list.
        - E (str, optional): Center-of-mass energy. Default is '13TeV'. Other options are '13p6TeV' and '14TeV.'
        - ReweightSChan (bool, optional): Flag to enable s-channel reweighting. Default is False.
        - RefMassRelWidth (tuple, optional): Reference mass and relative width for s-channel reweighting. Default is an empty tuple.
        - mass_widths_dict (dictionary, optional): allows different width values to be specified for each mass point. The dictionary should have a structure like {600: [0.01,0.02], 1000 : [0.01, 0.02, 0.03]}, where the keys are the mass points and the lists are the relative widths for these mass points
        """

        #check if the specified energy is correct
        if E not in ['13TeV','13p6TeV','14TeV']:
            raise Exception("Invalid center-of-mass energy specified. Please specify '13TeV', '13p6TeV', or '14TeV'")

        if ReweightSChan and not (isinstance(RefMassRelWidth, tuple) and len(RefMassRelWidth) == 2):
            raise Exception("When reweighting the s-channel process you need to specify the reference mass and relative width of the MC sample you are weighting")

        loc = os.path.realpath(os.path.dirname(__file__))
        self.hel_map = {}

        hel_file = open('%(loc)s/../rwgt_%(E)s/rw_me/SubProcesses/MadLoop5_resources/ML5_0_HelConfigs.dat' % vars(), 'r')
        for i, line in enumerate(hel_file): 
            self.hel_map[tuple([int(h) for h in line.split()])] = i+1 
        hel_file.close()

        self.me_vals = {}
        self.weights = {}

        
        if mass_widths_dict: self.mass_widths_dict = mass_widths_dict
        else: self.mass_widths_dict = {}
        # add additional mass and width points to dictionary if masses and widths lists are specified 
        for m in masses:
            if m not in self.mass_widths_dict: self.mass_widths_dict[m] = widths
            else: 
                for w in widths: self.mass_widths_dict[m].append(w)

        a = 0.785398
        self.ca = math.cos(a)
        self.sa = math.sin(a)

        self.params_map = {}
        if ReweightSChan:
            ref_mass = RefMassRelWidth[0]
            ref_relwidth = RefMassRelWidth[1]
            print("INFO: Reweighting S-channel process with mass=%g and width=%g" % (ref_mass,ref_relwidth))
            card = loc+'/../cards/params_card_sH_only.dat'
            mass_width_name = ('Mass_%g_RelWidth_%g' % (ref_mass,ref_relwidth)).replace('.','p')
            temp_card = '%(loc)s/../cards/temp/param_card_sH_only_%(mass_width_name)s.dat' % vars()
            self.MakeParmsFile(card,temp_card,ref_mass,ref_relwidth)
            self.params_map['ref'] = temp_card
        else:
            print("INFO: Reweighting SM process")
            self.params_map['ref'] = loc+'/../cards/param_card_sm.dat'

        self.params_map['box'] = loc+'/../cards/param_card_box.dat'
        self.params_map['box_and_schannel_h_1'] = loc+'/../cards/param_card_box_and_schannel_h_1.dat'
        self.params_map['box_and_schannel_h_2'] = loc+'/../cards/param_card_box_and_schannel_h_2.dat'

        self.weights_names = ['box','schannel_h','box_and_schannel_h_i']

        os.system('mkdir -p %s/../cards/temp/' % loc)
        for mass, widths in self.mass_widths_dict.items():
            for width in widths:
                formatted_width = ('%g' % width) if width >= 0.001 else ('%.10f' % width).rstrip('0')
                mass_width_name = ('Mass_%g_RelWidth_%s' % (mass,formatted_width)).replace('.','p')
                #mass_width_name = ('Mass_%g_RelWidth_%g' % (mass,width)).replace('.','p')
                self.weights_names += ['schannel_H_%s' % mass_width_name, 'box_and_schannel_H_i_%s' % mass_width_name, 'schannel_H_and_schannel_h_i_%s' % mass_width_name]
                for c in ['box_and_schannel_H_1', 'box_and_schannel_H_2', 'all']:
                    card = '%(loc)s/../cards/param_card_%(c)s.dat' % vars()
                    temp_card = '%(loc)s/../cards/temp/param_card_%(c)s_%(mass_width_name)s.dat' % vars() 
                    self.MakeParmsFile(card,temp_card,mass,width)
                    self.params_map['%(c)s_%(mass_width_name)s' % vars()] = temp_card

        self.rw = {}
        for name, params in self.params_map.items():
            # Initialising the params cards is very slow so it is quicker to make copies of the reweighting module, one for each params card
            # This way the initialisation only needs to be done once per params card rather than once per event per params card 
            if not os.path.exists('%(loc)s/../rwgt_%(E)s/rw_me_temp_%(name)s' % vars()): 
                os.system('cp -r %(loc)s/../rwgt_%(E)s/rw_me %(loc)s/../rwgt_%(E)s/rw_me_temp_%(name)s' % vars())
            if sys.version_info.major == 2:
                self.rw[name] = imp.load_module('allmatrix2py', *imp.find_module('%(loc)s/../rwgt_%(E)s/rw_me_temp_%(name)s/SubProcesses/allmatrix2py' % vars()))
                del sys.modules['allmatrix2py']
            elif sys.version_info.major == 3:
                module_dir = '%(loc)s/../rwgt_%(E)s/rw_me_temp_%(name)s/SubProcesses' % vars()
                module_pattern = os.path.join(module_dir, "allmatrix3py*.so")
                
                # Find the correct .so file (handles different Python versions/architectures)
                matching_files = glob.glob(module_pattern)
                
                if not matching_files:
                    raise ImportError(f"No matching shared library found for pattern: {module_pattern}")
                
                module_path = matching_files[0]  # Use the first match
                module_name = "allmatrix3py"
                
                spec = importlib.util.spec_from_file_location(module_name, module_path)
                module = importlib.util.module_from_spec(spec)
                
                # Store the module with a unique name
                self.rw[name] = module
                del sys.modules['allmatrix3py']
            else:
                raise Exception("Unsupported Python version, currently only python2 and python3 are supported")
            self.rw[name].set_madloop_path('%(loc)s/../rwgt_%(E)s/rw_me_temp_%(name)s/SubProcesses/MadLoop5_resources' % vars())
            self.rw[name].initialise(params)


    def GetWeightsDescription(self, weight_name):
        """
        Print a short description of the reweighting for a given weight.

        Parameters:
        - weight_name (str): The name of the weight for which to provide a description.

        Returns:
        str: A short description of the reweighting associated with the given weight.
        """

        out = ''
        split_name = weight_name.split('_')
        for i, x in enumerate(split_name):
            if x == 'Mass':
                mass = split_name[i+1] 
            if x == 'RelWidth':
                width = split_name[i+1].replace('p','.')

        if weight_name == 'box':
            out = 'Reweight to non-resonant including only box diagram (kappa_t=kappa_b=1, lambda_hhh=0)'
        elif weight_name == 'schannel_h':
            out = 'Reweight to non-resonant including only s-channel diagram'
        elif weight_name == 'box_and_schannel_h_i': 
            out = 'Reweight to the non-resonant interference contribution, (interference between box and s-channel diagrams)'
        elif weight_name.startswith('schannel_H_Mass'):
            out = 'Reweight to the resonant s-channel contribution (s-channel for heavy Higgs). Heavy Higgs mass = %s and relative width = %s' % (mass, width)
        elif weight_name.startswith('box_and_schannel_H_i_Mass'):
            out = 'Reweight to the interference contribution (interference between box and s-channel diagrams). Heavy Higgs mass = %s and relative width = %s' % (mass, width)
        elif weight_name.startswith('schannel_H_and_schannel_h_i_Mass'):
            out = 'Reweight to the interference contribution (interference between s-channel diagrams for light and heavy Higgs). Heavy Higgs mass = %s and relative width = %s' % (mass, width)
        return out

    def GetLHEHeader(self):
        """
        Print the header to be included in the modified LHE file.
        """

        header =  '<weightgroup name=\'HHReweighter\'>\n'
        for key in self.weights_names:
            header += '<weight id=\'%s\' > %s </weight>\n' % (key, self.GetWeightsDescription(key))
        header += '</weightgroup>\n'
        return header

    def GetWeightNames(self):
        return self.weights_names

    def MakeParmsFile(self, infile, outfile, mass, relwidth):
        """
        Copy and modify a parameter file by replacing placeholders with specified values.

        Parameters:
        - infile (str): The path to the input parameter file.
        - outfile (str): The path to the output parameter file.
        - mass (float): The mass value to be used in the replacement.
        - relwidth (float): The relative width value to be used in the replacement.
        """

        width = relwidth*mass

        with open(infile, 'r') as file:
            # Read the content of the file
            file_content = file.read()

        # Perform the replacement
        new_content = file_content.replace('$Weta', '%g' % width).replace('$Meta', '%g' % mass)

        with open(outfile, 'w') as file:
            # Write the modified content back to the file
            file.write(new_content)

    def invert_momenta(self,p):
        """
        Invert momenta order between Fortran and C-python. 
        Fortran/C-python do not order table in the same order.
        """
        return [[p[j][i] for j in range(len(p))] for i in range(len(p[0]))]
    
    
    # code to do lorentz boosts taken from: https://github.com/MatthewDKnight/nanoAOD-tools/blob/eventIDSkimming/python/postprocessing/modules/reweighting/standalone_reweight.py

    def rotZ(self, p, angle):
        p[1], p[2] = p[1]*math.cos(angle)-p[2]*math.sin(angle),  p[1]*math.sin(angle)+p[2]*math.cos(angle)
        return p

    def rotY(self, p, angle):
        p[1], p[3] = p[1]*math.cos(angle)+p[3]*math.sin(angle),  -p[1]*math.sin(angle)+p[3]*math.cos(angle)
        return p

    def allboost(self, p, pboost):
        """
        Strategy here is to rotate pboost such that it lies along the z axis. Then boost pboost 
        so that it is in its rest frame. Perform the same transformations to p, and then undo
        the rotations at the end.
        """
        p, pboost = copy.copy(p), copy.copy(pboost) #force pass by value
    
        #rotate around z axis such that there is no y component
        if pboost[1]!=0:
            z_rot_angle = math.atan2(pboost[2],pboost[1])
        elif pboost[2]>0:
            z_rot_angle = math.pi/2
        else:
            z_rot_angle = -math.pi/2
    
        p = self.rotZ(p, -z_rot_angle)
        pboost = self.rotZ(pboost, -z_rot_angle)
    
        #rotate around y axis so pboost lies along z axis
        r = math.sqrt(pboost[1]**2+pboost[2]**2+pboost[3]**2)
        y_rot_angle = math.acos(pboost[3]/r)
    
        p = self.rotY(p, -y_rot_angle)
        pboost = self.rotY(pboost, -y_rot_angle)
    
        #perform Lorentz boost
        p = self.zboost(p, pboost)
        pboost = self.zboost(pboost, pboost)
    
        #undo rotations
        p = self.rotY(p, y_rot_angle)
        pboost = self.rotY(pboost, y_rot_angle)
        p = self.rotZ(p, z_rot_angle)
        pboost = self.rotZ(pboost, z_rot_angle)
    
        if abs(p[1]) < 1e-6 * p[0]:
            p[1] = 0
        if abs(p[2]) < 1e-6 * p[0]:
            p[2] = 0
        if abs(p[3]) < 1e-6 * p[0]:
            p[3] = 0
        if abs(pboost[1]) < 1e-6 * pboost[0]:
            pboost[1] = 0
        if abs(pboost[2]) < 1e-6 * pboost[0]:
            pboost[2] = 0
        if abs(pboost[3]) < 1e-6 * pboost[0]:
            pboost[3] = 0
    
        #check that transformations put pboost in rest frame
        assert pboost[1]==pboost[2]==pboost[3]==0
    
        return p
    
    
    def zboost(self, part, pboost=[]):
        """Both momenta should be in the same frame.
           The boost perform correspond to the boost required to set pboost at
           rest (only z boost applied).
        """
        E = pboost[0]
        pz = pboost[3]
    
        #beta = pz/E
        gamma = E / math.sqrt(E**2-pz**2)
        gammabeta = pz  / math.sqrt(E**2-pz**2)
    
        out =  [gamma * part[0] - gammabeta * part[3],
                            part[1],
                            part[2],
                            gamma * part[3] - gammabeta * part[0]]
    
        if abs(out[3]) < 1e-6 * out[0]:
            out[3] = 0
        return out

    def CalcWeights(self):
        """
        Calculate the weights for the individual contributions (box, s-channels, and interference terms)
        """

        weights = {}

        ca = self.ca 
        sa = self.sa
        wt_box = self.me_vals['box']
        wt_schannel_h = ((self.me_vals['box_and_schannel_h_2']-self.me_vals['box']) - 10.*(self.me_vals['box_and_schannel_h_1']-self.me_vals['box']))/90. 
        wt_box_and_schannel_h_i = self.me_vals['box_and_schannel_h_1'] - wt_box - wt_schannel_h

        # account for mixing angle used to compute MEs
        weights['box'] = wt_box/ca**4
        weights['schannel_h'] = wt_schannel_h/ca**2 
        weights['box_and_schannel_h_i'] = wt_box_and_schannel_h_i/ca**3

        for mass, widths in self.mass_widths_dict.items():
            for width in widths:
                formatted_width = ('%g' % width) if width >= 0.001 else ('%.10f' % width).rstrip('0')
                mass_width_name = ('Mass_%g_RelWidth_%s' % (mass,formatted_width)).replace('.','p')
                #mass_width_name = ('Mass_%g_RelWidth_%g' % (mass,width)).replace('.','p')
                wt_schannel_H = ((self.me_vals['box_and_schannel_H_2_%s' % mass_width_name]-wt_box) - 10.*(self.me_vals['box_and_schannel_H_1_%s' % mass_width_name]-self.me_vals['box']))/90.
                wt_box_and_schannel_H_i = self.me_vals['box_and_schannel_H_1_%s' % mass_width_name] - wt_box - wt_schannel_H
                wt_all = self.me_vals['all_%s' % mass_width_name]
                wt_schannel_H_and_schannel_h_i = wt_all - wt_box - wt_schannel_H - wt_schannel_h - wt_box_and_schannel_H_i - wt_box_and_schannel_h_i

                # account for mixing angle used to compute MEs
                weights['schannel_H_%s' % mass_width_name] = wt_schannel_H/sa**2
                weights['box_and_schannel_H_i_%s' % mass_width_name] = wt_box_and_schannel_H_i/(ca**2*sa)
                weights['schannel_H_and_schannel_h_i_%s' % mass_width_name] = wt_schannel_H_and_schannel_h_i/(ca*sa)

        if 'ref' not in self.me_vals:
            raise Exception("No reference ME was specified for the reweighting") 

        for key in weights:
            weights[key] = weights[key]/self.me_vals['ref']

        self.weights = weights

        return weights

    def ReweightEvent(self, parts, alphas=0.118, hels=None):
        """
        Perform event reweighting based on the particle content.

        Parameters:
        - parts (list): A list of particles, where each particle is specified as [pdgid, E, px, py, pz].
        - alphas (float, optional): The strong coupling constant value. Default is 0.118.
        - hels (list, optional): List of helicity states corresponding to each particle. If provided,
          then sum over all the helicity states.

        Returns:
        list: List of calculated weights for the event.
        """

        # make sure 4-vectors stored as double precision 
        parts_double = []
        for i, part in enumerate(parts):
            for j in range(1,5):
                parts[i][j] = float(part[j])

        # sort the particles by pdgid so that gluons are given first then the Higgs bosons
        if hels and len(hels) == len(parts):
            parts, hels = zip(*sorted(zip(parts,hels), key=lambda x: x[0]))
            parts = list(parts)
            hels = list(hels)
        else: 
            parts = sorted(parts, key=lambda x: x[0])

        nhel = -1
        if hels and len(hels) == 4 and tuple(hels) in self.hel_map:
            nhel = self.hel_map[tuple(hels)]

        mode = -1

  
        if len(parts) == 4 and parts[0][0] == 21 and parts[1][0] == 21 and parts[2][0] == 25 and parts[3][0] == 25: 
            # if 2 gluons and 2 higgs bosons are included then this is likely reweighting LO events with the full information about the incoming and outgoing particles
            # Checks for E/momentum conservation between incoming and outgoing, if momentum is not conserved then default to mode=2
            mom_diff = [parts[0][i] + parts[1][i] - parts[2][i] - parts[3][i] for i in range(1,5)]
            if True in [ abs(m) >= 1e-02 for m in mom_diff]: mode = 2  
            else: mode = 1

        if mode != 1 and sum(1 for part in parts if part[0] == 25) == 2:
            # if gluons are not provided, or if more than 4 particles are specified (e.g an additional jet from an NLO sample), then we default to approximate method (useful for NLO or LO with missing generator information is missing)
            # in this case we boost to the di-Higgs rest frame, construct incoming gluons in this rest frame, and sum over helicity states 

            mode = 2
            higgs = [part for part in parts if part[0] == 25]    

            pboost = [higgs[0][i] + higgs[1][i] for i in range(1,5)] 
    
            higgs_boosted_4vec = []
            for part in higgs:
                if (pboost[1]!=0) or (pboost[2]!=0): # if non-zero pt boost do boost in all directions
                    higgs_boosted_4vec.append(self.allboost(part[1:], pboost))
                else: # otherwise just boost along z
                    higgs_boosted_4vec.append(self.zboost(part[1:], pboost))
      
            # now we construct our 'artificial' gluons
            g1 = [21, higgs_boosted_4vec[0][0], 0., 0., higgs_boosted_4vec[0][0]] 
            g2 = [21, higgs_boosted_4vec[0][0], 0., 0., -higgs_boosted_4vec[0][0]] 
           
            h1 = [25] + higgs_boosted_4vec[0]
            h2 = [25] + higgs_boosted_4vec[1]

            parts = [g1, g2, h1, h2]
            nhel = -1


        if mode not in [1,2]:
            raise Exception("The particle content should include at least two Higgs bosons")

        pdgids = []
        part_4vecs = []
        for part in parts:
            pdgids.append(part[0])
            part_4vecs.append(part[1:])
        
        # Shift all higgs masses to 125 GeV to match the value used in the ME code
        for i, part_4vec in enumerate(part_4vecs):
            if pdgids[i] == 25:
                # the ME method assumes outgoing Higgs bosons have mass = 125 GeV
                # the masses of the Higgs bosons are modified to guarantee that this is the case
                # if a mass difference > 1 GeV is found, then a warning is printed so users can check their implementation is correct
                mass = (part_4vec[0]**2 - part_4vec[1]**2 - part_4vec[2]**2 - part_4vec[3]**2)**.5
                mass_diff = abs(mass-float(125))
                if mass_diff > 0. :
                    if mass_diff > 1.:
                        print('WARNING: found an outgoing Higgs boson with mass = %g. '
                              'The reweighting method expects outgoing Higgs bosons to have mass = 125 GeV. '
                              'The masses of all outgoing Higgs bosons will be changed to 125 GeV. '
                              'You should check if you implementation is correct and that this is appropriate for your use case.' % mass)
                    part_4vec[0] = (float(125)**2 + part_4vec[1]**2 + part_4vec[2]**2 + part_4vec[3]**2)**.5

        #TODO: Add checks for energy/momentum conservation
        part_4vecs = self.invert_momenta(part_4vecs)
    
        scale2 = 0.

        for name, params in self.params_map.items():
            val = self.rw[name].smatrixhel(pdgids,-1,part_4vecs,alphas,scale2,nhel)
            self.me_vals[name] = val[0]


        self.CalcWeights()
      
        return self.weights

if __name__ == '__main__':

    rw = HHReweight([600,650],[0.01,0.05])
    

    print ('Testing a LO event reweighting:')

    alphas = 1.03955600e-01
    
    parts = [
      [21, 2.7425261217e+01, 0.0000000000e+00, 0.0000000000e+00, 2.7425261217e+01],
      [21, 1.5506245631e+03, 0.0000000000e+00, 0.0000000000e+00, -1.5506245631e+03],
      [25, 8.1365468547e+02, -2.5093111382e+01, 1.6194796557e+02, -7.8711634426e+02],
      [25, 7.6439513889e+02, 2.5093111382e+01, -1.6194796557e+02, -7.3608295767e+02],
    ]
    spinup = [1, 1, 0, 0]
    
    print('Exact weights:') 
    
    weights = rw.ReweightEvent(parts,alphas,hels=spinup)
    print(weights)
    
    print('Approx weights:') 
    parts = [
      [25, 8.1365468547e+02, -2.5093111382e+01, 1.6194796557e+02, -7.8711634426e+02],
      [25, 7.6439513889e+02, 2.5093111382e+01, -1.6194796557e+02, -7.3608295767e+02]
    ]
    weights = rw.ReweightEvent(parts) 
    print(weights)
    
    print ('Testing an NLO event reweighting:')

    alphas = 1.72049E-01
    
    parts = [
      [21, 1.879630199E+02, 0., 0., 1.879630199E+02],
      [21, 2.961593627E+02, 0., 0., -2.961593627E+02],
      [25, 2.091732996E+02, -2.723168606E+01,  1.551842338E+02,  5.748702593E+01],
      [25, 2.473527517E+02, 1.020773483E+01, -1.571700601E+02, -1.440547053E+02],
      [21, 2.759633129E+01, 1.702395123E+01,  1.985826331E+00, -2.162866337E+01]
    ]

    print ('Passing only Higgs boson\'s:')
    weights = rw.ReweightEvent(parts[2:4])
    print(weights)

    print ('Passing Higgs boson\'s and initial state gluons:')
    weights = rw.ReweightEvent(parts[0:4])
    print(weights)

    print ('Passing Higgs boson\'s and initial and final state gluons:')
    weights = rw.ReweightEvent(parts)
    print(weights)
