# This example shows how to produce a set of ME weights for several points in the singlet model 

from python.reweight import HHReweight
import math
import csv
import numpy as np
from scipy.interpolate import UnivariateSpline

class HHReweightSingletModel(HHReweight):

    def __init__(self, param_points, E='13TeV', ReweightSChan=False, RefMassRelWidth=()):
        """
        Initialize the reweighting module with specified parameters.

        Parameters:
        - param_points (list): List of parameter points for reweighting. The list should be arranged like: [[mH,sina,tanb]].
        - E (str, optional): Center-of-mass energy. Default is '13TeV'. Other options are '13p6TeV' and '14TeV.'
        - ReweightSChan (bool, optional): Flag to enable s-channel reweighting. Default is False.
        - RefMassRelWidth (tuple, optional): Reference mass and relative width for s-channel reweighting. Default is an empty tuple.
        """

        self.mh = 125. # SM-like Higgs mass

        # Use partial widths provided in YR4 (https://twiki.cern.ch/twiki/pub/LHCPhysics/LHCHWG/Higgs_XSBR_YR4_update.xlsx) for SM-like Higgs
        # for all partial widths except for H->hh
        # create spline that we can use to get width for any value of mH
        data = np.genfromtxt('inputs/YR4_BSM_Width.txt', delimiter=' ')
        mass = data[1:, 0]
        width_noHhh_data = np.sum(data[1:, 1:], axis=1)
        self.width_noHhh_spline = UnivariateSpline(mass, width_noHhh_data, s=0)

        self.param_points = []
        mass_widths_dict = {}
        weights_names_mod_dep = []
        for point in param_points:
            mH = point[0]
            sina = point[1]
            tanb = point[2]
            width = self.Width(sina, tanb, mH)
            rel_width = width/mH # the reweighting module expects the relative width rather than the absolute value
            if mH in mass_widths_dict: mass_widths_dict[mH].append(rel_width)
            else: mass_widths_dict[mH] = [rel_width] 
            self.param_points.append([mH, sina, tanb, rel_width])
            point_name = ('singlet_mH_%g_sina_%g_tanb_%g' % (mH, sina, tanb)).replace('.','p')
            weights_names_mod_dep.append(point_name)  
            weights_names_mod_dep.append(point_name+'_no_sH') 
            weights_names_mod_dep.append(point_name+'_sH') 
        HHReweight.__init__(self, masses=[], widths=[], E=E, ReweightSChan=ReweightSChan, RefMassRelWidth=RefMassRelWidth, mass_widths_dict=mass_widths_dict)
        self.weights_names = weights_names_mod_dep

    def GetWeightsDescription(self, weight_name):
        """
        Print a short description of the reweighting for a given weight.

        Parameters:
        - weight_name (str): The name of the weight for which to provide a description.

        Returns:
        str: A short description of the reweighting associated with the given weight.
        """

        out = ''
        split_name = weight_name.split('_')
        for i, x in enumerate(split_name):
            if x == 'mH':
                mH = split_name[i+1] 
            if x == 'sina':
                sina = split_name[i+1].replace('p','.')
            if x == 'tanb':
                tanb = split_name[i+1].replace('p','.')                

        if weight_name.startswith('singlet_'):
            if weight_name.endswith('_no_sH'):
                out = 'Reweight to the the singlet model excluding the resonant s-channel contribution (s-channel for heavy Higgs) for mH = %s, sina = %s, and tanb = %s. Note only the s-channel contribution is excluded, all other terms including inteferences between the s-channel are still included.' % (mH, sina, tanb)
            elif weight_name.endswith('_sH'):
                out = 'Reweight to the resonant s-channel contribution (s-channel for heavy Higgs) for the singlet model with mH = %s, sina = %s, and tanb = %s' % (mH, sina, tanb)
            else:     
                out = 'Reweight to the full di-Higgs model for the singlet model with mH = %s, sina = %s, and tanb = %s' % (mH, sina, tanb)

        return out        
    
    def kappa_h_t(self, sina):
        """
        Return the Yukawa coupling modifier for the h (SM-like scalar).
    
        Parameters:
        - sina (float)
        """
        return math.cos(math.asin(sina))
    
    def kappa_H_t(self, sina):
        """
        Return the Yukawa coupling modifier for the H (BSM scalar).
    
        Parameters:
        - sina (float)
        """
        return sina
    
    def kappa_lambda_hhh(self, sina, tanb):
        """
        Return the coupling modifier for the lambda_{hhh} trilinear coupling (relative to lambda_SM).
    
        Parameters:
        - sina (float)
        - tanb (float)
        """
        cosa = math.cos(math.asin(sina))
        return cosa**3 - tanb*sina**3
    
    
    def kappa_lambda_Hhh(self, sina, tanb, mH):
        """
        Return the coupling modifier for the lambda_{Hhh} trilinear coupling (relative to lambda_SM).
    
        Parameters:
        - sina (float)
        - tanb (float)
        - mH (float): the BSM Higgs mass
        """
        cosa = math.cos(math.asin(sina))
        return (2*self.mh**2 + mH**2)/self.mh**2 * (cosa**2*sina + tanb*sina**2*cosa)
    
    def Width_Hhh(self, sina, tanb, mH, mh=125.):
        """
        Return the H->hh partial width
    
        Parameters:
        - sina (float)
        - tanb (float)
        - mH (float): the BSM Higgs mass
        """
        kap = self.kappa_lambda_Hhh(sina, tanb, mH)
        v=246.
        lam_SM = mh**2/(2*v)
        return (kap*lam_SM)**2*(1.-4*(self.mh/mH)**2)**.5/(8*math.pi*mH)
    
    def Width(self, sina, tanb, mH):
        """
        Return the total width
    
        Parameters:
        - sina (float)
        - tanb (float)
        - mH (float): the BSM Higgs mass
        """
    
        # get the H->hh partial width
        width_Hhh = self.Width_Hhh(sina, tanb, mH)
        # get the width for the other channels
        # these are equal to the SM widths scaled down universally by sina**2
        width_noHhh = self.width_noHhh_spline(mH)*sina**2
        # sum to get the total width
        return width_Hhh+width_noHhh  

    def ReweightEvent(self, parts, alphas=0.118, hels=None):
        """
        Perform event reweighting based on the particle content.

        Parameters:
        - parts (list): A list of particles, where each particle is specified as [pdgid, E, px, py, pz].
        - alphas (float, optional): The strong coupling constant value. Default is 0.118.
        - hels (list, optional): List of helicity states corresponding to each particle. If provided,
          then sum over all the helicity states.

        Returns:
        list: List of calculated weights for the event.
        """   

        # return the weights for the individual templates scaled to kap=1
        weights = HHReweight.ReweightEvent(self,parts, alphas, hels)

        weights_mod_dep = {}

        for point in self.param_points:
            mH = point[0]
            sina = point[1]
            tanb = point[2]
            rel_width = point[3]
            point_name = ('singlet_mH_%g_sina_%g_tanb_%g' % (mH, sina, tanb)).replace('.','p')
            mass_width_name = ('Mass_%g_RelWidth_%g' % (mH,rel_width)).replace('.','p')

            kappa_h_t = self.kappa_h_t(sina)
            kappa_H_t = self.kappa_H_t(sina)
            kappa_h_lam = self.kappa_lambda_hhh(sina, tanb)
            kappa_H_lam = self.kappa_lambda_Hhh(sina, tanb, mH)

            wt_box = weights['box']*kappa_h_t**4
            wt_schannel_h = weights['schannel_h']*kappa_h_t**2*kappa_h_lam**2
            wt_box_and_schannel_h_i = weights['box_and_schannel_h_i']*kappa_h_t**3*kappa_h_lam
            wt_schannel_H = weights['schannel_H_%s' % mass_width_name]*kappa_H_t**2*kappa_H_lam**2
            wt_box_and_schannel_H_i = weights['box_and_schannel_H_i_%s' % mass_width_name]*kappa_h_t**2*kappa_H_t*kappa_H_lam
            wt_schannel_H_and_schannel_h_i = weights['schannel_H_and_schannel_h_i_%s' % mass_width_name]*kappa_H_t*kappa_H_lam*kappa_h_t*kappa_h_lam

            # we store the weight including all contributions except for the sH
            # this is so the HH non-resonant background samples can be reweighted exclusing the H->hh resonant contribution
            wt_nosH = wt_box+wt_schannel_h+wt_box_and_schannel_h_i+wt_box_and_schannel_H_i+wt_schannel_H_and_schannel_h_i
            wt_full = wt_nosH+wt_schannel_H

            weights_mod_dep[point_name] = wt_full
            weights_mod_dep[point_name+'_no_sH'] = wt_nosH
            weights_mod_dep[point_name+'_sH'] = wt_schannel_H
    
        return weights_mod_dep

if __name__ == '__main__':

    # parameter points specified like mH, sina, tanb

    param_points = [
        [260, 0.24, 3.5], 
        [260, 0.16, 3.5],     
    ]

    rw = HHReweightSingletModel(param_points)

    print ('Testing a LO event reweighting for singlet model:')

    alphas = 1.03955600e-01
    
    parts = [
      [21, 2.7425261217e+01, 0.0000000000e+00, 0.0000000000e+00, 2.7425261217e+01],
      [21, 1.5506245631e+03, 0.0000000000e+00, 0.0000000000e+00, -1.5506245631e+03],
      [25, 8.1365468547e+02, -2.5093111382e+01, 1.6194796557e+02, -7.8711634426e+02],
      [25, 7.6439513889e+02, 2.5093111382e+01, -1.6194796557e+02, -7.3608295767e+02],
    ]
    spinup = [1, 1, 0, 0]
    
    print('Weights:') 
    
    weights = rw.ReweightEvent(parts,alphas,hels=spinup)
    print(weights)

    
    
    