import math
import argparse
import os
import gzip
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),os.pardir))
from python.reweight import *
from python.reweight_singlet import *


parser = argparse.ArgumentParser()
parser.add_argument('--input', '-i', help= 'LHE file to be reweighted')
parser.add_argument('--output', '-o', help= 'Output LHE file with weights added')
parser.add_argument('--ReweightSChan', help= 'Reweight MC generated for S-channel process', action='store_true')
parser.add_argument('--RefMassRelWidth', help= 'Mass and relative width of S-channel process reference sample should have the form "600,0.01"')

args = parser.parse_args()

# initialise reweighting
# We will add 2 sets of weights in this example
# The first set of weights come from the "model independent" approach, where weights are stores assuming all couplings equal to the SM values
# For the heavy H this means that the Yukawa coupling is equal to the Yukawa coupling for the SM h, and that the Hhh triple Higgs couplings is equal to the SM lambda
# In this approach the masses and widths you want to consider also have to be specified.
# The second set of weights come from the model dependent approach for the singlet model.
# In this approach you set the model parameters for the singlet model which are mH, sina, and tanb.

# model dependent parameter points
moddep_param_points = [   
    [600, 0.17, 1.5], 
    [600, 0.08, 1.5],     
]

if args.ReweightSChan:
    # "model independent" reweighting
    ref_mass = float(args.RefMassRelWidth.split(',')[0])
    ref_width = float(args.RefMassRelWidth.split(',')[1]) 
    rw = HHReweight([ref_mass],[0.01,0.0083,0.02],ReweightSChan=args.ReweightSChan,RefMassRelWidth=(ref_mass,ref_width))
    # model dedependent reweighting (singlet model)
    rw_moddep = HHReweightSingletModel(ReweightSChan=args.ReweightSChan,RefMassRelWidth=(ref_mass,ref_width),param_points=moddep_param_points)

else: 
    # "model independent" reweighting
    rw = HHReweight([600,650],[0.01,0.0083,0.062])
    # model dedependent reweighting (singlet model)
    rw_moddep = HHReweightSingletModel(param_points=moddep_param_points)

header = rw.GetLHEHeader()
header_moddep = rw_moddep.GetLHEHeader()
header = '\n'.join(header.split('\n')[:-2]+header_moddep.split('\n')[1:])

# Open the LHE file for reading
lhe_filename = args.input
compressed_input = lhe_filename.endswith('.gz')

if compressed_input: lhe_file = gzip.open(lhe_filename, 'rt')
else: lhe_file = open(lhe_filename, "r")

out_file = open(args.output, "w")

# Parse the LHE file and fill the TTree
event_started = False
count=0

contains_initrwgt = False
for line in lhe_file:
    if '</initrwgt>' in line: contains_initrwgt = True 
    if '<event>' in line: break
# go back to start of the file
lhe_file.seek(0)

def GetWeightsStr(parts,alphas,hels):
    weights = rw.ReweightEvent(parts,alphas,hels)
    weights_mod_dep = rw_moddep.ReweightEvent(parts,alphas,hels)
    # combine model dependent and model independent weights
    weights = dict(weights.items()+weights_mod_dep.items())
    full_weights_str = ''
    for key in dict(weights.items()+weights_mod_dep.items()):
        wt = weights[key]
        weight_str = format(wt, "e")
        if wt > 0: weight_str='+'+weight_str
        full_weights_str += '<wgt id=\'%s\'> %s </wgt>\n' % (key, weight_str)
    return full_weights_str

for line in lhe_file:
    line = line.strip()

    if line.startswith("<event>"):
        event_started = True
        if count % 10000 == 0: print("Processing %ith event" % count)
        count+=1

        parts = []
        hels = []
        alphas = 0.118
        weights_written = False

    # TODO: Add header if "</header>" not already included
    elif (contains_initrwgt and line.startswith("</initrwgt>")) or (not contains_initrwgt and  line.startswith("</header>")):
        if not contains_initrwgt: out_file.write('<initrwgt>\n')
        out_file.write(header)
        if not contains_initrwgt: out_file.write('</initrwgt>\n')

    elif line.startswith('</rwgt>') and not weights_written:
        # store weight info
        out_file.write(GetWeightsStr(parts,alphas,hels))
        weights_written = True

    elif line.startswith("</event>"):
        # end of event
        # compute weights and store variables
        event_started = False

        # store weight info
        if not weights_written:
            out_file.write('<rwgt>\n')
            out_file.write(GetWeightsStr(parts,alphas,hels))
            out_file.write('</rwgt>\n')
            weights_written = True

    elif event_started:
        parts_info = line.split()
        if len(parts_info) == 6 and not line.startswith("<"):
            # read in alphas
            alphas = float(parts_info[5])
        if len(parts_info) == 13:
            # read in particle information
            part = [int(parts_info[0]), float(parts_info[9]),float(parts_info[6]),float(parts_info[7]),float(parts_info[8])]
            hel = float(parts_info[12])
            parts.append(part)
            hels.append(hel)
    out_file.write(line+'\n')

lhe_file.close()
out_file.close()
